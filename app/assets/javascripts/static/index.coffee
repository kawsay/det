$(document).on 'turbolinks:load', ->
    
    # Loads only on static#index
    return unless page.controller() == 'static' && page.action() == 'index'

    $(document).ready ->
        $(".filter-button").click -> 
            value = $(this).attr("data-filter")
            console.log value
            # Display the selected category, hide others
            if (value == "all") 
                $(".filter").parent().show("3000").fadeIn("3000")
            else
                console.log($('.filter').not('.'+value).parent().length)
                $(".filter").not("."+value).parent().hide("1000").fadeOut("1000");
                $(".filter").filter("."+value).parent().show("3000").fadeIn("3000");

            # Highlight selected button
            $('.filter-button').removeClass('btn-light').addClass('btn-outline-light')
            $(this).removeClass('btn-outline-light').addClass('btn-light')

