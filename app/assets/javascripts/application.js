// This is a manifest file that'll be compiled into application.js, which will include all the files
// listed below.
//
// Any JavaScript/Coffee file within this directory, lib/assets/javascripts, or any plugin's
// vendor/assets/javascripts directory can be referenced here using a relative path.
//
// It's not advisable to add code directly here, but if you do, it'll appear at the bottom of the
// compiled file. JavaScript code in this file should be added after the last require_* statement.
//
// Read Sprockets README (https://github.com/rails/sprockets#sprockets-directives) for details
// about supported directives.
//
//= require rails-ujs
//= require activestorage
//= require jquery3
//= require popper
//= require bootstrap-sprockets
//= require turbolinks
//= require_tree .

$(document).on('turbolinks:load', function() {

  // When creating an album, allow user to dynamically remove tracks
  $('form').on('click', '.remove_record', function(event) {
    $(this).prev('input[type=hidden]').val('1');
    $(this).closest('tr').hide();
    return event.preventDefault();
  });
  // When creating an album, allow user to dynamically add tracks
  $('form').on('click', '.add_fields', function(event) {
    let regex, time;
    time  = new Date().getTime();
    regex = new RegExp($(this).data('id'), 'g');
    $('.fields').append($(this).data('fields').replace(regex, time));
    return event.preventDefault();
  });

  // Stop video (YT/Vimeo) when modal is closed by reseting <iframe> `src`
  $('.modal').on('hidden.bs.modal', function () {
    videoIframe     = this.children[0].children[0].children[0].children[0]
    videoSrc        = videoIframe.src
    videoIframe.src = ''
    videoIframe.src = videoSrc
    console.log($('.modal:eq(4)'))
  })
});