class AlbumsController < ApplicationController
  before_action :current_user_is_admin?, except: [:index, :show]
  before_action :set_album, only: [:show, :edit, :update, :destroy]

  def index
    @albums = Album.all.includes(:tracks)
  end

  def show
    @album = Album.find(params[:id])
  end

  def new
    @album = Album.new
    @album.tracks.build
  end

  def edit
    @album.tracks.build
  end

  def create
    @album = Album.new(album_params)

    respond_to do |format|
      if @album.save
        format.html { redirect_to @album, flash: { success: "Album added" } }
        format.json {  }
      else
        format.html { render action: :new, flash: { error: "Something wrong happened :(" } }
        format.json {  }
      end
    end
  end

  def update
    @album.update(album_params)

    respond_to do |format|
      if @album.save
        format.html { redirect_to @album, flash: { success: "Album edited" } }
        format.json {  }
      else
        format.html { redirect_back fallback_location: root_path, flash: { error: "Something wrong happened :(" } }
        format.json {  }
      end
    end
  end

  def destroy
    @album.destroy
    
    redirect_to albums_path, flash: { success: 'Album deleted' }
  end

	private

  def album_params
    params.require(:album).permit(:name, :release_date, :album_artwork, :url, tracks_attributes: Track.attribute_names.map(&:to_sym).push(:_destroy))
  end

  def set_album
    @album = Album.find(params[:id])
  end
end
