class SamplepacksController < ApplicationController
  before_action :current_user_is_admin?, except: [:index, :show]
  before_action :set_samplepack, only: [:show, :edit, :update, :destroy]

  def index
    @samplepacks = Samplepack.all
  end

  def show
  end

  def new
    @samplepack = Samplepack.new
  end

  def create
    @samplepack = Samplepack.new(samplepack_params)
    respond_to do |format|
      if @samplepack.save
        format.html { redirect_to @samplepack, flash: { success: "Samplepack added !"} }
        format.json {   }
      else
        format.html { render action: :new, flash: { error: "Something wrong happened :(" } }
        format.json {   }
      end
    end
  end

  def edit
  end

  def update
    respond_to do |format|
      if @samplepack.update(samplepack_params)
        format.html { redirect_to samplepacks_path, flash: { success: 'Samplepack updated' } }
        format.json {  }
      else
        format.html { render action: :edit, flash: { notice: 'Something wrong happened :(' } }
        format.json {  }
      end
    end
  end

  def destroy
    @samplepack.destroy
    redirect_to samplepacks_path, flash: { success: 'Samplepack deleted' }
  end

  private

  def samplepack_params
    params.require(:samplepack).permit(:name, :release_date, :price, :album_artwork, :url)
  end

  def set_samplepack
    @samplepack = Samplepack.find(params[:id])
  end
end