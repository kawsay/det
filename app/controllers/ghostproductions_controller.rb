class GhostproductionsController < ApplicationController
  before_action :current_user_is_admin?, except: [:index, :show]
  before_action :set_ghostproduction, only: [:show, :edit, :update, :destroy]

  def index
    @ghostproductions = Ghostproduction.all
  end 

  def show
  end

  def new
    @ghostproduction = Ghostproduction.new
  end

  def create
    @ghostproduction = Ghostproduction.new(ghostproduction_params)
    respond_to do |format|
      if @ghostproduction.save
        format.html { redirect_to @ghostproduction, flash: { success: "Ghostproduction added !" } }
        format.json {   }
      else
        format.html { render action: :new, flash: { error: "Something wrong happened :(" } }
        format.json {   }
      end
    end
  end

  def edit
  end

  def update
    respond_to do |format|
      if @ghostproduction.update(ghostproduction_params)
        format.html { redirect_to @ghostproduction, flash: { success: "Ghostproduction added" } }
        format.json {   }
      else
        format.html { render action: :new, flash: { error: "Something wrong happened :(" } }
        format.json {   }
      end
    end
  end

  def destroy
    @ghostproduction.destroy
    redirect_to ghostproduction_path, flash: { success: "Ghostproduction deleted" }
  end

  private
  
  def ghostproduction_params
    params.require(:ghostproduction).permit(:name, :url, :description)
  end

  def set_ghostproduction
    @ghostproduction = Ghostproduction.find(params[:id])
  end
end

