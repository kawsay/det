class StaticController < ApplicationController
  def index
    @resources = [
      Album.all.with_attached_album_artwork,
      Samplepack.all.with_attached_album_artwork,
      Demo.all,
      Ghostproduction.all
    ].flatten
  end
end
