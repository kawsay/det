class TracksController < ApplicationController
  before_action :current_user_is_admin?, except: [:index, :show]
  before_action :set_track, only: [:show, :edit, :update, :destroy]

  def index
    @tracks = Track.all
  end

  def show
  end

  def new
    @track = Track.new
  end
  
  def create
    @track = Track.new(track_params)
    respond_to do |format|
      if @track.save
        format.html { redirect_to root_path, notice: 'Track added' }
        format.json {  }
      else
        format.html { redirect_to root_path, notice: 'Error' }
        format.json {  }
      end
    end
  end

  def edit
    @albums = Album.includes(:name).all
  end

  def update
    respond_to do |format|
      if @track.update(track_params)
        format.html { redirect_to @track, notice: 'Track updated' }
        format.json {  }
      else
        format.html { redirect_to root_path, notice: 'Error' }
        format.json {  }
      end
    end
  end

  def destroy
    @track.destroy
    redirect_to tracks_path, flash: { success: 'Track deleted' }
  end

  private

  def track_params
    params.require(:track).permit(:name, urls_attributes: Url.attribute_names.map(&:to_sym).push(:_destroy))
  end

  def set_track
    @track = Track.find(params[:id])
  end
end