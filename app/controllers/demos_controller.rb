class DemosController < ApplicationController
  before_action :current_user_is_admin?, except: [:index, :show]
  before_action :set_demo, only: [:show, :edit, :update, :destroy]

  def index
    @demos = Demo.all
  end

  def show
    @image = MiniMagick::Image.open(@demo.video_thumbnail)
  end

  def new
    @demo = Demo.new
  end

  def create
    @demo = Demo.new(demo_params)
    respond_to do |format|
      if @demo.save
        format.html { redirect_to @demo, flash: { success: "Demo added !" } }
        format.json {  }
      else
        format.html { render action: :new, flash: { error: "Something wrong happened :(" } }
        format.json {  }
      end
    end
  end

  def edit
  end

  def update
    respond_to do |format|
      if @demo.update(demo_params)
        format.html { redirect_to demos_path, flash: { success: 'Demo updated' } }
        format.json {  }
      else
        format.html { render action: :edit, flash: { notice: 'Something wrong happened :(' } }
        format.json {  }
      end
    end
  end

  def destroy
    @demo.destroy
    redirect_to demos_path, flash: { success: 'Demo deleted' }
  end

  private

  def demo_params
    params.require(:demo).permit(:name, :url, :album_artwork)
  end

  def set_demo
    @demo = Demo.find(params[:id])
  end
end
