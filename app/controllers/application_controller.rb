class ApplicationController < ActionController::Base

  protected

  def current_user_is_admin?
    current_user.try(:admin?) ? true : redirect_to(root_path)
  end
end
