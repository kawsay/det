module ApplicationHelper
  # Display errors with bootstrap classes
  def flash_class(key)
    case key
      when 'notice'  then "alert alert-info"
      when 'success' then "alert alert-success"
      when 'error'   then "alert alert-error"
      when 'alert'   then "alert alert-error"
    end
  end

  # Build a track prototype associated to an album
  # Allow user to create and associate tracks to an album while creating it
  def link_to_add_track(name, f, association, **args)
    new_object = f.object.send(association).klass.new
    id = new_object.object_id
    fields = f.fields_for(association, new_object, child_index: id) do |builder|
      render('tracks/form_new', f: builder)
    end
    link_to(name, '#', class: 'add_fields' + args[:class], data: {id: id, fields: fields.gsub("\n", "")})
  end

  # Build an URL prototype associated to a track
  def link_to_add_url(name, f, association, **args)
    new_object = f.object.send(association).klass.new
    id = new_object.object_id
    fields = f.fields_for(association, new_object, child_index: id) do |builder|
      render('urls/form', f: builder)
    end
    link_to(name, '#', class: 'add_fields' + args[:class], data: {id: id, fields: fields.gsub("\n", "")})
  end

  # From a link, display the target website logo and set the `href` to the link passed as argument
  def display_track_link(link)
    if link.include?('soundcloud')
      return link_to(image_tag('/logos/soundcloud.png', class: 'img-fluid logo'), link.html_safe)
    elsif link.include?('bandcamp')
      return link_to(image_tag('/logos/bandcamp.png', class: 'img-fluid logo'), link.html_safe)
    elsif link.include?('spotify')
      return link_to(image_tag('/logos/spotify.png', class: 'img-fluid logo'), link.html_safe)
    elsif link.include?('deezer')
      return link_to(image_tag('/logos/deezer.png', class: 'img-fluid logo'), link.html_safe)
    end
  end

  # Render a partial correponding to the item class, each having specification
  def display_mosaic(item)

  puts "====================== #{item}"

    if item.class    == Demo
      return render('demos/hoverable_demo', demo: item)
    elsif item.class == Samplepack
      return render('samplepacks/hoverable_samplepack', samplepack: item)
    elsif item.class == Album
      return render('albums/hoverable_album', album: item)
    elsif item.class == Ghostproduction
      return render('ghostproductions/hoverable_ghostproduction', ghostproduction: item)
    end
  end

  # Admin button
  def action_button(text, action)
    if user_signed_in?
      if current_user.admin == true
        button_to text, url_for(action), method: 'get', class: "btn btn-outline-primary float-right mr-6 ml-2"
      end
    end
  end
  
  # Admin delete button
  def delete_button(text, object)
    if user_signed_in?
      if current_user.admin == true
        button_to text, object, method: 'delete', class: "btn btn-outline-danger float-right mr-6 ml-2", data: {confirm: "Are you sure?"}
      end
    end
  end

  # Admin link
  def action_link(text, action)
    if current_user.try(:admin?)
      link_to text, url_for(action), method: 'get'
    end
  end

  # Admin delete link
  def delete_link(text, action)
    if current_user.try(:admin?)
      link_to text, url_for(action), method: 'get', class: 'text-danger', data: {confirm: "Are you sure?"}
    end
  end

end
