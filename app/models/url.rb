class Url < ApplicationRecord
  belongs_to :track, optional: true
end
