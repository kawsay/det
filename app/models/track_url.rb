class TrackUrl < ApplicationRecord
  belongs_to :track
  belongs_to :url
end
