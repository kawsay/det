class Track < ApplicationRecord
    # has_many :cart_items, as: :cartable
    # has_many :carts, through: :cart_items, source: :cart
    belongs_to :album, optional: true, touch: true
    has_many :urls, dependent: :destroy
    accepts_nested_attributes_for :urls, allow_destroy: true, reject_if: proc { |att| att['link'].blank? }

    # accept_nested_attributes_for :

    has_one_attached :audio_file
end
