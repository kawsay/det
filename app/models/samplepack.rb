class Samplepack < ApplicationRecord
  include NamableConcern
  include ArtworkableConcern

  has_many :samplepack_sample, dependent: :destroy
  has_many :samples, through: :samplepack_sample, dependent: :destroy

  has_many :cart_items, as: :cartable
  has_many :carts, through: :cart_items, source: :cart


  validates :release_date, presence:   { message: ': samplepack should have a release date.' } 
  validates :url,          presence:   { message: ': samplepack should have an URL' }
  validates :price,        presence:   { message: ': samplepack should have a price' }
end

