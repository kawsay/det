class Album < ApplicationRecord
  include NamableConcern
  include ArtworkableConcern

  has_many :tracks, dependent: :destroy
  accepts_nested_attributes_for :tracks, allow_destroy: true, reject_if: proc { |att| att['name'].blank? }

  # has_many :cart_items, as: :cartable
  # has_many :carts, through: :cart_items, source: :cart

  validates :release_date, presence:   { message: ': album should have a release date.' }
end

