class Sample < ApplicationRecord
  has_many :cart_items, as: :cartable
  has_many :carts, through: :cart_items, source: :cart
  has_many :samplepack_samples, dependent: :delete_all, touch: true
  has_many :samplepacks, through: :samplepack_samples, dependent: :delete_all, touch: true

  has_one_attached :audio_file
  has_one_attached :album_artwork
end
