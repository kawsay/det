class Cart < ApplicationRecord
  has_many :cart_items, dependent: :destroy
  has_many :samplepacks, through: :cart_items, source: :cartable, source_type: 'Samplepack'
  has_many :samples, through: :cart_items, source: :cartable, source_type: 'Sample'
  has_many :albums, through: :cart_items, source: :cartable, source_type: 'Album'
  has_many :tracks, through: :cart_items, source: :cartable, source_type: 'Track'
end
