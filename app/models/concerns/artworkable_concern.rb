module ArtworkableConcern
  extend ActiveSupport::Concern
  include Rails.application.routes.url_helpers

  included do
    has_one_attached :album_artwork
    validate :has_an_album_artwork_attached
  end

  def has_an_album_artwork_attached
    unless self.album_artwork.attached?
      errors.add(:album_artwork, ": samplepack shoud have an artwork")
    end
  end

  def large
    rails_representation_path(self.album_artwork.variant(resize: '600x600').processed, only_path: true)
  end

  def mosaic
    return self.album_artwork.variant(resize: '200x200').processed.service_url
  end

#  def medium
#    return self.album_artwork.variant(resize: '200x200').processed
#  end
end
