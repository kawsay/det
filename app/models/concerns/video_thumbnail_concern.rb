module VideoThumbnailConcern
  extend ActiveSupport::Concern

  included do
    # Callbacks download, resize and save video thumbnail with the following process :
    #   -> #fetch_thumbnail  : analyzes the submitted URL and fetch the corresponding 'API'
    #   -> #square_thumbnail : crops the image as a square, centering the content (=> TO IMPROVE)
    #   -> #save_thumbnail   : saves the thumbnail into ActiveStorage
    #
    # #square_thumbnail saves images into the public folder. I may consider using /tmp folder
    has_one_attached :square_image
    before_validation :fetch_thumbnail, on: [:create, :update]
    before_create :square_thumbnail, :save_thumbnail, on: [:create, :update]
    validates :url,             presence: { message: ': URL field is empty' }
    validates :video_thumbnail, presence: { message: ': couldn\'t generate an image with this URL' }
    validate :url_is_an_iframe
  end

  def url_is_an_iframe
    self.url.start_with?('<iframe') && self.url.end_with?('</iframe>')
  end

  def large
    MiniMagick::Image.open(self.video_thumbnail).resize('600x600')
  end

  def mosaic
    return self.video_thumbnail.variant(resize: '200x200').processed
  end

  private

    # Find out if the video comes from Youtube or Vimeo,
    # then get the video ID,
    # and finaly fetch the URI/API to get a link to the thumbnail
    def fetch_thumbnail
      if self.url.include?('youtube')
        id = self.url.match(/\/[a-zA-Z0-9\-_]{11}/).to_s[1..-1]
        self.video_thumbnail = "https://img.youtube.com/vi/#{id}/mqdefault.jpg"
      elsif self.url.include?('vimeo')
        id = self.url.match(/[\d]{8,9}/).to_s
        json = open("http://vimeo.com/api/v2/video/#{id}.json")
        self.video_thumbnail = JSON.parse(json.string)[0]['thumbnail_large']      
      end
    end
  
    # Name the thumbnail according to the resource (Demo/Ghostproduction) name
    # Use .created_at in case 2 resources use the same name
    # => my_favorite_demo_20190912_093951
    def thumbnail_name
      self.name.gsub(' ','_').downcase + '_' + self.created_at.strftime('%Y%m%d_%H%M%S')
    end
  
    # Images are store in /public directory, using thumbnail_name
    def thumbnail_path
      Rails.root.join('public', 'video_thumbnail/').to_s + self.thumbnail_name
    end
  
    # TO IMPROVE
    # Generates a square image from any dimensions
    # Saves files appending its width to thumbnail_name
    def square_thumbnail(width = '600')
      img = MiniMagick::Image.open(self.video_thumbnail)
  
      img.combine_options do |i|
        i.resize "#{width}x#{width}^"
        i.gravity(:center)
        i.extent "#{width}x#{width}"
      end
      img.write "#{self.thumbnail_path}"
    end
  
    # Save the downloaded and squared thumbnail
    def save_thumbnail
      self.square_image.attach(io: File.open(self.thumbnail_path), 
                               filename: self.thumbnail_name,
                               content_type: 'image/jpeg')
    end
end

