module NamableConcern
  extend ActiveSupport::Concern

  included do
    validates :name, presence:   { message: ': should be filled' }
    validates :name, uniqueness: { message: ': should be unique' }
    validates :name, format: { with: /\A[a-zA-Z0-9\-_\ ]+\z/, message: ': special chars allowed: "_", "-" and " "' }
    validates :name, length:     { minimum: 2, too_short: ": 2 characters minimum" }
    validates :name, length:     { maximum: 20, too_long: ": 20 characters maximum" }
  end
end

