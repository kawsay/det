class User < ApplicationRecord
  has_one :cart, dependent: :destroy
  has_many :cart_items, through: :cart, dependent: :destroy
  has_many :samples, through: :cart_items, source: :cartable, source_type: 'Sample'
  has_many :tracks, through: :cart_items, source: :cartable, source_type: 'Track'

  # Include default devise modules. Others available are:
  # :confirmable, :lockable, :timeoutable, :trackable and :omniauthable
  devise :database_authenticatable, :registerable,
         :recoverable, :rememberable, :validatable
end
