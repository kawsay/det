class SamplepackSample < ApplicationRecord
  belongs_to :samplepack, touch: true
  belongs_to :sample, touch: true
end
