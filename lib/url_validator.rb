module UrlValidator
  def url_is_an_iframe
    self.url.start_with?('<iframe ') && self.url.end_with?('</iframe>')
  end
end

