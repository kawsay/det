Rails.application.routes.draw do
  devise_for :users
  # For details on the DSL available within this file, see http://guides.rubyonrails.org/routing.html
  root to: 'static#index'

  resources :tracks
  resources :static
  resources :samplepacks
  resources :albums
  resources :demos,            path: :sound_design
  resources :ghostproductions, path: :band_production
  # resources :carts, only: [:edit]
  
  get '/contact/', to: 'static#contact', as: 'contact'
end
