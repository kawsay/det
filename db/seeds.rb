# This file should contain all the record creation needed to seed the database with its default values.
# The data can then be loaded with the rails db:seed command (or created alongside the database with db:setup).
#
# Examples:
#
#   movies = Movie.create([{ name: 'Star Wars' }, { name: 'Lord of the Rings' }])
#   Character.create(name: 'Luke', movie: movies.first)

print 'Cleaning DB...'
[User, Sample, Samplepack, Track, Album].each(&:destroy_all)
puts "\u2713"

print "Creating foobar...   "
User.create!(
  email:    "foo@bar.com",
	password: "foobar",
	admin:		"true"
	)
	puts "ᕕ( ᐛ )ᕗ"
	
	# print 'Creating Albums...'
	# 3.times do
	# 	a = Album.create!(
	# 		name:	        Faker::Name.name,
	# 		release_date:	Faker::Date.backward(1000),
	# 	)
	# 	a.album_artwork.attach(io: File.open('public/dogo.png'), filename: 'dogo.png')
	# end
	# puts "\u2713"

	# print 'Creating Tracks...'
	# 10.times do
	# 	t = Track.create!(
	# 		name:	    Faker::Name.name,
	# 		album_id:	Album.all.sample.id
	# 	)
	# 	t.audio_file.attach(io: File.open('public/test_sound.wav'), filename: 'test_sound.wav')
	# end
	# puts "\u2713"

	# print 'Creating Samples...'
	# 10.times do
	# 	s = Sample.create!(
		# 		name:	        Faker::Name.name,
# 		release_date:	Faker::Date.backward(1000),
# 		price:        rand(400)
# 	)
# 	s.audio_file.attach(io: File.open('public/test_sound.wav'), filename: 'test_sound.wav')
# 	s.album_artwork.attach(io: File.open('public/dogo.png'), filename: 'dogo.png')
# end
# puts "\u2713"

# print 'Creating Samplepacks...'
# 3.times do
# 	Samplepack.create!(
# 		name:	        Faker::Name.name,
# 		release_date:	Faker::Date.backward(1000),
# 		price:        rand(400)
# 	)
# end
# puts "\u2713"

# print 'Creating SamplepackSamples...'
# 10.times do
# 	SamplepackSample.create!(
#     sample_id:      Sample.all.sample.id,
#     samplepack_id:  Samplepack.all.sample.id
# 	)
# end
# puts "\u2713"


# print 'Creating Users...'
# 9.times do
# 	User.create!(
# 		email:    Faker::Internet.email,
# 		password: "133742"
# 		)
# 	end
# puts "\u2713"



# print 'Creating Carts...'
# 10.times do |i|
# 	Cart.create!(
# 		user_id: User.all[i].id
# 	)
# end
# puts "\u2713"

# print 'Creating CartItems (Samplepacks, Samples, Albums, Tracks)...'
# 10.times do
#   CartItem.create!(
#     cart_id: 		      Cart.all.sample.id,
#     cartable_type:    "Samplepack",
#     cartable_id: 	    Samplepack.all.sample.id
#   )
# end
# 20.times do
# 	CartItem.create!(
#     cart_id: 		      Cart.all.sample.id,
# 		cartable_type:    "Sample",
# 		cartable_id: 	    Sample.all.sample.id
#   )
# end
# 10.times do
#   CartItem.create!(
#     cart_id: 		      Cart.all.sample.id,
#     cartable_type:    "Album",
#     cartable_id: 	    Album.all.sample.id
#   )
# end
# 20.times do
#   CartItem.create!(
#     cart_id: 		    Cart.all.sample.id,
#     cartable_type: 	"Track",
#     cartable_id:    Track.all.sample.id
#   )
# end
# puts "\u2713"