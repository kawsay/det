class RemoveDescriptionAndPriceToAlbum < ActiveRecord::Migration[5.2]
  def change
    remove_columns :albums, :description, :price
  end
end
