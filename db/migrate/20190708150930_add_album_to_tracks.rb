class AddAlbumToTracks < ActiveRecord::Migration[5.2]
  def change
    add_reference :tracks, :album, foreign_key: true
    remove_column :tracks, :price, :release_date
  end
end
