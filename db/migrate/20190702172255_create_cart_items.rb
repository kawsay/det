class CreateCartItems < ActiveRecord::Migration[5.2]
  def change
    create_table :cart_items do |t|
      t.references :cart, foreign_key: true
      t.integer :cartable_id
      t.string :cartable_type

      t.timestamps
    end
    add_index :cart_items, [:cartable_id, :cartable_type]
  end
end
