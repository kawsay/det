class CreateGhostproductions < ActiveRecord::Migration[5.2]
  def change
    create_table :ghostproductions do |t|
      t.string :name
      t.string :url

      t.timestamps
    end
  end
end
