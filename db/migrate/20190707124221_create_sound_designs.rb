class CreateSoundDesigns < ActiveRecord::Migration[5.2]
  def change
    create_table :sound_designs do |t|
      t.string :name
      t.string :url

      t.timestamps
    end
  end
end
