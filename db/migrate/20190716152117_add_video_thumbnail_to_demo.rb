class AddVideoThumbnailToDemo < ActiveRecord::Migration[5.2]
  def change
    add_column :demos, :video_thumbnail, :string
  end
end
