class CreateSamplepackSamples < ActiveRecord::Migration[5.2]
  def change
    create_table :samplepack_samples do |t|
      t.references :samplepack, foreign_key: true
      t.references :sample, foreign_key: true

      t.timestamps
    end
  end
end
