class AddUrlToSamplepacks < ActiveRecord::Migration[5.2]
  def change
    add_column :samplepacks, :url, :string
  end
end
