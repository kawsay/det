class CreateTracks < ActiveRecord::Migration[5.2]
  def change
    create_table :tracks do |t|
      t.string :name
      t.date :release_date
      t.integer :price

      t.timestamps
    end
  end
end
