class AddDescriptionToGhostproduction < ActiveRecord::Migration[5.2]
  def change
    add_column :ghostproductions, :description, :text
  end
end
