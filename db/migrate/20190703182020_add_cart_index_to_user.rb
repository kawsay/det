class AddCartIndexToUser < ActiveRecord::Migration[5.2]
  def change
    add_reference :users, :cart, index: { unique: true }
  end
end
