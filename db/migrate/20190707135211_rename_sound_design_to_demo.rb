class RenameSoundDesignToDemo < ActiveRecord::Migration[5.2]
  def change
    rename_table :sound_designs, :demos
  end
end
