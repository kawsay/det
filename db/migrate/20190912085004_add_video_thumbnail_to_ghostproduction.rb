class AddVideoThumbnailToGhostproduction < ActiveRecord::Migration[5.2]
  def change
    add_column :ghostproductions, :video_thumbnail, :string
  end
end
